-- SUMMARY --

Phoneword is a simple module that provides a block displaying a click-to-call
telephone link that cross-fades between a phone word and its corresponding
phone number. Clicking the link will launch the user's phone dialler (if
installed).

Phonewords are mnemonic phrases represented as alphanumeric equivalents of a
telephone number, commonly a vanity number like 1800 FLOWERS. Phonewords are
memorable and effective, but it's also handy to have the actual number
displayed as some cultures and users are less familiar with this practice and
some phone keypads don't have letters associated with each number.

For a full description of the module, visit the project page:
  https://www.drupal.org/sandbox/inteja/2633760

To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/2633760


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Add the Phoneword block to a region of your theme and configure it to suit.
  You can adjust phone word, phone number, the number that is dialled when
  clicked, the mouse over tool-tip and the cross-fade timing.

* The default CSS is minimal but there's plenty of scope to add your own
  styling. The cross-fading between the phone word and number works best
  with monospaced fonts.
