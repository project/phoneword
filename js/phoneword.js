/**
 * @file
 * Provides cross-fade transition for phoneword module.
 *
 * @type {Drupal~behavior}
 *
 * @prop {Drupal~behaviorAttach} attach
 *   Setup and commence cross-fade transition cycle.
 */

(function ($) {
  'use strict';

  Drupal.behaviors.phoneword = {
    attach: function (context, settings) {

      // Get settings.
      var element = settings.phoneword.element;
      var fadeInterval = settings.phoneword.fadeInterval;
      var fadeDuration = settings.phoneword.fadeDuration;

      // Set initial state.
      $(element + ' div', context).fadeOut(0);
      $(element + ' div:first', context).fadeIn(0).addClass('active');

      // Start crossfade cycle.
      setInterval(function () {
        var active = $(element + ' div.active', context);
        var next = active.next();
        if (next.length === 0) {
          next = $(element + ' div:first', context);
        }
        active.removeClass('active').fadeOut(parseInt(fadeDuration));
        next.addClass('active').fadeIn(parseInt(fadeDuration));
      }, fadeInterval);

    }

  };

})(jQuery);
